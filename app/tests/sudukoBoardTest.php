<?php
use PHPUnit\Framework\TestCase;
require dirname(__DIR__) . '/vendor/autoload.php';

class SudokuBoardTest extends TestCase
{
    private \SudokuLogic\SudokuBoard $sudokuBoard;
    
    protected function setUp() : void
    {
        $this->sudokuBoard = new SudokuLogic\SudokuBoard();
    }

    public function testEmptySudokuBoardCellsForNull()
    {
        for($row = 0; $row <= 8; $row++)
        {
            for($column = 0; $column <= 8; $column++)
            {
                $this->assertEquals(NULL, $this->sudokuBoard->getCellValue($row, $column));
            }
        }
    }

    public function testInvalidRow()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->sudokuBoard->getCellValue(-1, 1);
    }

    public function testInvalidColumn()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->sudokuBoard->getCellValue(0, 9);
    }

    public function testSetCellValue()
    {
        $this->sudokuBoard->setCellValue(0, 0, 9);
        $this->assertEquals(9, $this->sudokuBoard->getCellValue(0, 0));
    }

    public function testSetTwoCellValues()
    {
        $this->sudokuBoard->setCellValue(0, 0, 9);
        $this->sudokuBoard->setCellValue(8, 8, 3);
        $this->assertEquals(9, $this->sudokuBoard->getCellValue(0, 0));
        $this->assertEquals(3, $this->sudokuBoard->getCellValue(8, 8));
        $this->assertEquals(NULL, $this->sudokuBoard->getCellValue(5, 5));
    }
    
    public function testIfMissingOneValueInRowFillsInMissingValue()
    {
        for($column = 0; $column < 8; $column++)
        {
            $this->sudokuBoard->setCellValue(1, $column, $column + 1);
        }
        $this->sudokuBoard->checkCellForPossibleValues(1, 8);
        $this->assertEquals(9, $this->sudokuBoard->getCellValue(1, 8));
    }
}
