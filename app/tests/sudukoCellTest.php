<?php
use PHPUnit\Framework\TestCase;
require dirname(__DIR__) . '/vendor/autoload.php';

class SudokuCellTest extends TestCase
{
    private \SudokuLogic\SudokuCell $sudokuCell;

    protected function setUp() : void
    {
        $this->sudokuCell = new SudokuLogic\SudokuCell();
    }

    public function testNewCellKnownValue()
    {
        $this->assertEquals(NULL, $this->sudokuCell->getKnownValue());
    }

    public function testChangeCellKnownValueToValidNumber()
    {
        $this->sudokuCell->setKnownValue(2);
        $this->assertEquals(2, $this->sudokuCell->getKnownValue());
    }

    public function testChangeCellKnownValueToInvalidNumber()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->sudokuCell->setKnownValue(10);
    }
}
