<?php
namespace SudokuLogic;
use InvalidArgumentException;
class sudokuCell
{
    private $knownValue = NULL;

    public function getKnownValue()
    {
        return $this->knownValue;
    }

    public function setKnownValue($knownValue)
    {
        if ($knownValue < 1 || $knownValue > 9)
        {
            throw new InvalidArgumentException(
                "Known Value of a sudoku cell must be between 1 and 9",
                $this->knownValue);
        }
        $this->knownValue = $knownValue;
    }
}
