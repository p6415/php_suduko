<?php
namespace SudokuLogic;
use InvalidArgumentException;
class SudokuBoard
{
    private array $cellValueArray = array(array());

    public function __construct()
    {
        $this->cellValueArray = array_fill(0, 9, array_fill(0, 9, NULL));
    }

    public function getCellValue(int $rowNumber, int $columnNumber)
    {
        $this->throwExceptionIfCellIndexInvalid($rowNumber, "Row");
        $this->throwExceptionIfCellIndexInvalid($columnNumber, "Column");
        return $this->cellValueArray[$rowNumber][$columnNumber];
    }

    public function setCellValue(int $rowNumber, int $columnNumber, int $cellValue)
    {
        $this->cellValueArray[$rowNumber][$columnNumber] = $cellValue;
//        $this->checkCellForPossibleValues($rowNumber, $columnNumber);
    }

    private function throwExceptionIfCellIndexInvalid(int $cellIndex, string $indexName)
    {
        if ($cellIndex < 0 || $cellIndex > 8)
        {
            throw new InvalidArgumentException("{$indexName} Number must
                between 0 and 8");
        }
    }
    
    public function checkCellForPossibleValues(int $row, int $column)
    {
        $alreadyUsedValues = array();
        for ($columnToCheck = 0; $columnToCheck < 9; $columnToCheck++)
        {
            if($column != $columnToCheck)
            {
                $valueForCurrentlyCheckedCell = $this->getCellValue($row, $columnToCheck);
                if($valueForCurrentlyCheckedCell != NULL)
                {
                    $alreadyUsedValues[] = $valueForCurrentlyCheckedCell;
                }
            }
        }
        $alreadyUsedValues = array_unique($alreadyUsedValues);
        if(sizeof($alreadyUsedValues) == 8)
        {
            for($rowToCheck = 1; $rowToCheck < 10; $rowToCheck++)
            {
                if(!in_array($rowToCheck, $alreadyUsedValues))
                {
                    break;
                }
            }
            $this->setCellValue($row, $column, $rowToCheck);
        }
    }
}
